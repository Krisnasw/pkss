<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'account_id', 'role_id', 'images', 'deleted_at', 'phone', 'username', 'confirmed', 'registered', 'notify_send', 'notify_paid', 'notify_viewed', 'force_pdfjs', 'city_id'
    ];

    protected $attributes = [
        'account_id' => 1
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($password)
    {   
        $this->attributes['password'] = bcrypt($password);
    }

    public function roles()
    {
        return $this->belongsTo('App\Role', 'role_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id', 'city_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permit extends Model
{
    //
    protected $table = 'permits';
    protected $fillable = ['permit_access'];

    public $timestamps = false;
}

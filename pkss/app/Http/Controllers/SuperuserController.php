<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Access;
use Auth;
use Alert;
use Validator;
use DB;
use App\User;
use App\City;
use App\Role;

class SuperuserController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->id;

            $akses = Access::getUserAccess($this->user,1);

            $this->permit = $akses->permit_access;
                
            if($akses->permit_access == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data = User::orderBy('first_name', 'asc')->get();
        return view('superuser.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $role = Role::all();
        $city = City::orderBy('city_name')->get();
        return view('superuser.create', compact('role', 'city'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'email' => 'required|email',
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'password' => 'required',
            'roles' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {

            $user = User::create([
                'username' => $request->email,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => $request->password,
                'city_id' => $request->city,
                'role_id' => $request->roles
            ]);
            
            if ($user) {
                # code...
                Alert::success('Superuser Berhasil Dibuat', 'Success');
                return redirect('home/user');
            } else {
                Alert::error('Gagal Membuat Superuser', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        return view('superuser.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $data = User::where('id', base64_decode($id))->first();
        $role = Role::all();
        $city = City::orderBy('city_name')->get();
        return view('superuser.edit', compact('data', 'role', 'city'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $valid = Validator::make($request->all(), [
            'email' => 'required|email',
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'password' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $user = User::findOrFail(base64_decode($id));
            $user->username = $request->email;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->password = $request->password;
            
            if ($user->save()) {
                # code...
                Alert::success('Superuser Berhasil Diupdate', 'Success');
                return redirect('home/user');
            } else {
                Alert::error('Gagal Update Superuser', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $del = User::findOrFail(base64_decode($id));
        if ($del->delete()) {
            # code...
            Alert::success('Superuser Berhasil Dihapus', 'Success');
            return redirect()->back();
        } else {
            Alert::error('Superuser Gagal Dihapus', 'Error');
            return redirect()->back();
        }
    }
}

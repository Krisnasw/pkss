<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Alert;
use Validator;
use DB;
use Access;
use App\Task;
use App\City;

class TaskController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->id;

            $akses = Access::getUserAccess($this->user,5);

            $this->permit = $akses->permit_access;
                
            if($akses->permit_access == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = DB::table('task_ro')
                    ->select('task_ro.*', 'a.title', 'b.name', 'b.ro_code')
                    ->join('task_types as a', 'a.id', 'task_ro.task_type_id')
                    ->join('register_officers as b', 'b.id', 'task_ro.user_id')
                    ->where('task_ro.is_deleted', 'N')
					->orderBy('task_ro.id', 'desc')
                    ->get();

        return view('task.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $type = DB::table('task_types')->where('is_deleted', 'N')->get();
		$inv = DB::table('invoices')->where('is_deleted', 0)->get();
        if (Auth::user()->roles->role_name == "Superuser" || Auth::user()->roles->role_name == "Admin Pusat" || Auth::user()->roles->role_name == "Staff Pusat") {
            # code...
            $city = City::all();
            $ro = DB::table('register_officers')->get();
            $client = DB::table('clients')->select('id', 'name')->get();
        } else {
            $ro = DB::table('register_officers')->where('city_id', Auth::user()->city_id)->get();
            $client = DB::table('clients')->select('id', 'name')->where('custom_value2', Auth::user()->city_id)->get();
            $uker = DB::table('uker')->select('id', 'uker_name')->where('city_id', Auth::user()->city_id)->get();
        }

        return view('task.create', compact('type', 'ro', 'client', 'city', 'inv'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'ro_id' => 'required',
            'task' => 'required',
            'client' => 'required',
            'date' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            if ($request->task == 3) {
                # code...
                DB::table('task_ro')->insert([
                    'user_id' => $request->ro_id,
                    'task_type_id' => $request->task,
                    'client_id' => $request->client,
                    'invoice_id' => ($request->invoice == 0) ? NULL : $request->invoice,
                    'for_date' => $request->date
                ]);
            } else {
                for ($i=0; $i < count($request->uker); $i++) {
                 # code...
                    DB::table('task_ro')->insert([
                        'user_id' => $request->ro_id,
                        'task_type_id' => $request->task,
                        'client_id' => $request->uker[$i],
                        'invoice_id' => ($request->invoice == 0) ? NULL : $request->invoice,
                        'for_date' => $request->date
                    ]);
                }
            }

            Alert::success('Tugas Berhasil Dibuat', 'Success');
            return redirect('home/task');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = DB::table('task_ro')
                    ->select('task_ro.*', 'b.name', 'b.ro_code', 'a.uker_name', 'b.id as ro_id')
                    ->join('uker as a', 'a.id', 'task_ro.client_id')
                    ->join('register_officers as b', 'b.id', 'task_ro.user_id')
                    ->where('task_ro.id', base64_decode($id))
                    ->first();

        $type = DB::table('task_types')->where('is_deleted', 'N')->get();
        $inv = DB::table('invoices')->where('is_deleted', 0)->get();
        if (Auth::user()->roles->role_name == "Superuser" || Auth::user()->roles->role_name == "Admin Pusat" || Auth::user()->roles->role_name == "Staff Pusat") {
            # code...
            $city = City::all();
            $ro = DB::table('register_officers')->get();
            $client = DB::table('clients')->select('id', 'name')->get();
        } else {
            $ro = DB::table('register_officers')->where('city_id', Auth::user()->city_id)->get();
            $client = DB::table('clients')->select('id', 'name')->where('custom_value2', Auth::user()->city_id)->get();
            $uker = DB::table('uker')->select('id', 'uker_name')->where('city_id', Auth::user()->city_id)->get();
        }
        return view('task.edit', compact('type', 'client', 'data', 'city', 'ro', 'uker', 'inv'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'task' => 'required',
            'date' => 'required',
            'uker' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            for ($i=0; $i < count($request->uker); $i++) {
                 # code...
                DB::table('task_ro')->where('id', base64_decode($id))->update([
                    'task_type_id' => $request->task,
                    'client_id' => $request->uker[$i],
                    'invoice_id' => ($request->invoice == 0) ? NULL : $request->invoice,
                    'for_date' => $request->date
                ]);
            }

            Alert::success('Tugas Berhasil Diupdate', 'Success');
            return redirect('home/task');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = DB::table('task_ro')->where('id', base64_decode($id))->limit(1)->update([
            'is_deleted' => 'Y'
        ]);

        if ($del) {
            # code...
            Alert::success('Tugas Berhasil Dihapus', 'Success');
            return redirect()->back();
        } else {
            Alert::error('Gagal Menghapus Tugas', 'Error');
            return redirect()->back();
        }
    }

    public function getListKanca(Request $request) {
        $inv = DB::table('clients')->where('custom_value2', $request->id)->where('is_deleted', 0)->get();

        $data = "<option value='0'>-- Pilih Kanca --</option>";
        foreach ($inv as $row) {
            $data .= "<option value='".$row->id."'>".$row->name."</option>";
        }

        $response['type']       = 'spbe';
        $response['content']    = $data;
        $response['param']      = '';
        return response($response);
    }

    public function getListRo(Request $request) {
        $inv = DB::table('register_officers')->where('client_id', $request->id)->where('is_deleted', 'N')->get();

        $data = "<option value='0'>-- Pilih RO --</option>";
        foreach ($inv as $row) {
            $data .= "<option value='".$row->id."'>".$row->ro_code." - ".$row->name."</option>";
        }

        $response['type']       = 'spbe';
        $response['content']    = $data;
        $response['param']      = '';
        return response($response);
    }

    public function getListUker(Request $request) {
        $inv = DB::table('ro_has_uker as a')->select('b.id', 'b.uker_name')->join('uker as b', 'b.id', 'a.uker_id')->where('a.ro_id', $request->id)->get();

        $data = "<option value='0'>-- Pilih Uker --</option>";
        foreach ($inv as $row) {
            $data .= "<option value='".$row->id."'>".$row->uker_name."</option>";
        }

        $response['type']       = 'spbe';
        $response['content']    = $data;
        $response['param']      = '';
        return response($response);
    }

    public function getInvoiceByClient(Request $request) {
        //
        $inv = DB::table('invoices')->where('client_id', $request->id)->where('is_deleted', 0)->get();

        $data = "<option value='0'>-- Pilih Invoice --</option>";
        foreach ($inv as $row) {
            $data .= "<option value='".$row->id."'>".$row->invoice_number."</option>";
        }

        $response['type']       = 'spbe';
        $response['content']    = $data;
        $response['param']      = '';
        return response($response);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Hash;
use Alert;
use Auth;
use DB;
use Validator;
use App\Task;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data = array();
        $data['ro'] = DB::table('register_officers')->count();
        $data['client'] = DB::table('clients')->count();
        $data['task'] = DB::table('task_ro')->count();
        $data['types'] = DB::table('task_types')->where('is_deleted', 'N')->count();

        $map = Task::where('status', 'Y')->join('register_officers as b', 'b.id', 'task_ro.user_id')->get()->toJson();
        return view('dashboard.index', compact('data', 'map'));
    }

    public function getEditProfile()
    {
        return view('dashboard.edit-profile');
    }

    public function doUpdateProfile(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $user = \DB::table('users')->where('id', Auth::user()->id)->limit(1)->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'phone' => $request->phone
            ]);

            if ($user) {
                # code...
                Alert::success('Profile Berhasil Diperbarui', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Memperbarui Profile', 'Error');
                return redirect()->back();
            }
        }
    }

    public function getChangePassword()
    {
        return view('dashboard.change-password');
    }

    public function doChangePassword(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required_with:new_password|same:new_password'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            if (Hash::check($request->old_password, Auth::user()->password)) {
                # code...
                $update = User::where('id', Auth::user()->id)->update([
                    'password' => bcrypt($request->confirm_password)
                ]);

                if ($update) {
                    # code...
                    Alert::success('Password Berhasil Diubah', 'Success');
                    return redirect()->back();
                } else {
                    Alert::error('Gagal Ubah Password', 'Error');
                    return redirect()->back();
                }
            } else {
                Alert::error('Password Lama Tidak Cocok', 'Error');
                return redirect()->back();
            }
        }
    }

    public function doLogout()
    {
        Auth::logout();
        Alert::success('Logout Berhasil', 'Success');
        return redirect('/');
    }
}

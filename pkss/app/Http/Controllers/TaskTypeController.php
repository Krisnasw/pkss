<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth;
use Alert;
use Validator;
use Access;
use App\TaskType;

class TaskTypeController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->id;

            $akses = Access::getUserAccess($this->user,4);

            $this->permit = $akses->permit_access;
                
            if($akses->permit_access == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data = TaskType::select('id','title','slug','is_deleted')->where('is_deleted', 'N')->get();
        return view('task_type.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('task_type.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'title' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back()->withInput($request->all());
        } else {
            $task = TaskType::create([
                'title' => $request->title,
                'slug' => str_slug($request->title)
            ]);

            if ($task) {
                # code...
                Alert::success('Jenis Tugas Berhasil Dibuat', 'Success');
                return redirect('home/jenis-tugas');
            } else {
                Alert::error('Gagal Membuat Jenis Tugas', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        return view('task_type.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $data = TaskType::where('id', base64_decode($id))->first();
        return view('task_type.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $valid = Validator::make($request->all(), [
            'title' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back()->withInput($request->all());
        } else {
            $task = TaskType::where('id', base64_decode($id))->update([
                'title' => $request->title,
                'slug' => str_slug($request->title)
            ]);

            if ($task) {
                # code...
                Alert::success('Jenis Tugas Berhasil Diupdate', 'Success');
                return redirect('home/jenis-tugas');
            } else {
                Alert::error('Gagal Update Jenis Tugas', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $del = TaskType::where('id', base64_decode($id))->update([
            'is_deleted' => 'Y'
        ]);

        if ($del) {
            # code...
            Alert::success('Jenis Tugas Berhasil Dihapus', 'Success');
            return redirect()->back();
        } else {
            Alert::error('Gagal Menghapus Jenis Tugas', 'Error');
            return redirect()->back();
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Alert;
use Validator;
use Access;
use File;
use DB;

class NewsController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->id;

            $akses = Access::getUserAccess($this->user,6);

            $this->permit = $akses->permit_access;
                
            if($akses->permit_access == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = DB::table('news')->get();
        return view('news.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'title' => 'required',
            'author' => 'required',
            'deskripsi' => 'required',
            'images' => 'image|mimes:jpg,jpeg,png'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
        } else {
            if ($request->hasFile('images')) {
                # code...
                $create = DB::table('news')->insert([
                    'title' => $request->title,
                    'slug' => str_slug($request->title),
                    'author' => $request->author,
                    'description' => $request->deskripsi,
                    'images' => Self::savePhoto($request->file('images'))
                ]);

                if ($create) {
                    # code...
                    Alert::success('Berita Berhasil Dibuat', 'Success');
                    return redirect('home/news');
                } else {
                    Alert::error('Gagal Membuat Berita', 'Error');
                    return redirect()->back();
                }
            } else {
                Alert::error('Gambar tidak ada', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = DB::table('news')->where('id', base64_decode($id))->first();
        return view('news.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'title' => 'required',
            'author' => 'required',
            'deskripsi' => 'required',
            'images' => 'image|mimes:jpg,jpeg,png'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
        } else {
            $data = DB::table('news')->where('id', base64_decode($id))->first();
            if ($request->hasFile('images')) {
                # code...
                Self::deletePhoto($data->images);
                $create = DB::table('news')->where('id', base64_decode($id))->update([
                    'title' => $request->title,
                    'slug' => str_slug($request->title),
                    'author' => $request->author,
                    'description' => $request->deskripsi,
                    'images' => Self::savePhoto($request->file('images'))
                ]);

                if ($create) {
                    # code...
                    Alert::success('Berita Berhasil Diupdate', 'Success');
                    return redirect('home/news');
                } else {
                    Alert::error('Gagal Update Berita', 'Error');
                    return redirect()->back();
                }
            } else {
                $create = DB::table('news')->where('id', base64_decode($id))->update([
                    'title' => $request->title,
                    'slug' => str_slug($request->title),
                    'author' => $request->author,
                    'description' => $request->deskripsi,
                    'images' => $data->images
                ]);

                if ($create) {
                    # code...
                    Alert::success('Berita Berhasil Diupdate', 'Success');
                    return redirect('home/news');
                } else {
                    Alert::error('Gagal Update Berita', 'Error');
                    return redirect()->back();
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = DB::table('news')->where('id', base64_decode($id))->delete();
        if ($del) {
            # code...
            Alert::success('Berita Berhasil Dihapus', 'Success');
            return redirect()->back();
        } else {
            Alert::error('Gagal Menghapus Berita', 'Error');
            return redirect()->back();
        }
    }

    protected function savePhoto($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'news';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $create['images'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $create['images'];
    }

    protected function deletePhoto($photo)
    {
        File::delete($photo);
        return $photo;
    }
}

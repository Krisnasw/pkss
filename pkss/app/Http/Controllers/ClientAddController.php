<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Alert;
use Validator;
use Access;
use DB;
use App\City;

class ClientAddController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->id;

            $akses = Access::getUserAccess($this->user,9);

            $this->permit = ($akses['permit_access']) ? $akses['permit_access'] : null;

            if($this->permit == null) {
                abort('401', 'Unauthorized action.');
            }

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = DB::table('client_additions as a')->select('a.*', 'b.name')->join('register_officers as b', 'b.id', 'a.ro_id')->get();
        return view('client-additions.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (Auth::user()->roles->role_name == "Superuser" || Auth::user()->roles->role_name == "Admin Pusat" || Auth::user()->roles->role_name == "Staff Pusat") {
            # code...
            $city = City::orderBy('city_name', 'asc')->get();
            $ro = DB::table('register_officers')->select('id', 'name')->get();
            return view('client-additions.create', compact('ro', 'city'));
        } else {
            $city = City::orderBy('city_name', 'asc')->get();
            $ro = DB::table('register_officers')->select('id', 'name')->where('city_id', Auth::user()->city_id)->get();
            $kanca = DB::table('clients')->select('id', 'name')->where('custom_value2', Auth::user()->city_id)->get();
            return view('client-additions.create', compact('ro', 'city', 'kanca'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'ro_id' => 'required',
            'realisasi' => 'required',
            'unrealisasi' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = DB::table('client_additions')->insert([
                'ro_id' => $request->ro_id,
                'realized' => $request->realisasi,
                'unrealized' => $request->unrealisasi
            ]);

            if ($create) {
                # code...
                Alert::success('Penambahan Client Berhasil', 'Success');
                return redirect('home/client-additions');
            } else {
                Alert::error('Gagal Melakukan Penambahan', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = DB::table('client_additions as a')->select('a.*', 'b.name')->join('register_officers as b', 'b.id', 'a.ro_id')->where('a.id', base64_decode($id))->first();
        return view('client-additions.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'realisasi' => 'required',
            'unrealisasi' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = DB::table('client_additions')->where('id', base64_decode($id))->limit(1)->update([
                'realized' => $request->realisasi,
                'unrealized' => $request->unrealisasi
            ]);

            if ($create) {
                # code...
                Alert::success('Penambahan Client Berhasil Diupdate', 'Success');
                return redirect('home/client-additions');
            } else {
                Alert::error('Gagal Melakukan Update Penambahan Client', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $del = DB::table('client_additions')->where('id', base64_decode($id))->delete();
        if ($del) {
            # code...
            Alert::success('Data Berhasil Dihapus', 'Success');
            return redirect()->back();
        } else {
            Alert::error('Gagal Menghapus Data', 'Error');
            return redirect()->back();
        }
    }
}

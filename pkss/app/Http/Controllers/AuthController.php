<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use Alert;
use App\User;

class AuthController extends Controller
{
    //
    public function getLogin(Request $request) 
    {
    	if (Auth::check()) {
    		# code...
    		return redirect('home/main');
    	}

    	return view('welcome');
    }

    public function doLogin(Request $request)
    {
    	$valid = Validator::make($request->all(), [
    		'email' => 'required',
    		'password' => 'required'
    	]);

    	if ($valid->fails()) {
    		# code...
    		Alert::info('Form Tidak Lengkap', 'Info');
    		return redirect()->back()->withInput($request->all());
    	} else {
    		$ifEmailExists = User::where('email', $request->email)->get();
    		if (count($ifEmailExists) <= 0) {
    			# code...
    			Alert::error('Email Tidak Terdaftar', 'Error');
    			return redirect()->back()->withInput($request->all());
    		} else {
    			if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
    				Alert::success('Login Berhasil', 'Success');
    				return redirect('home/main');
    			} else {
    				Alert::error('Email atau Password Salah', 'Error');
    				return redirect()->back()->withInput($request->all());
    			}
    		}
    	}
    }
}

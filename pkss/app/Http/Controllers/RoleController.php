<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Alert;
use Validator;
use Auth;
use DB;
use App\Role;
use App\Permit;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data = Role::get();
        $permit = DB::table('role_has_permit as a')->join('permits as b', 'b.id', 'a.permit_id')->join('role_pkss as c', 'c.id', 'a.role_id')->get();
        return view('role.index', compact('data', 'permit'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data = Permit::all();
        return view('role.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'name'=>'required',
            'permissions' =>'required',
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $role = Role::create([
                'role_name' => $request->name,
                'is_deleted' => 'N'
            ]);

            if (isset($request->permissions)) {
                # code...
                foreach ($request->permissions as $row) {
                    DB::table('role_has_permit')->insert([
                        'role_id' => $role->id,
                        'permit_id' => $row
                    ]);
                }
            }

            if ($role) {
                # code...
                Alert::success('Role Berhasil Dibuat', 'Success');
                return redirect('home/role');
            } else {
                Alert::error('Gagal Membuat Role', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        return redirect('home/roles');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $data = Role::findOrFail(base64_decode($id));
        return view('role.edit', compact('data', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $valid = Validator::make($request->all(), [
            'name'=>'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $role = Role::where('id', base64_decode($id))->update([
                'role_name' => $request->name
            ]);

            if ($role) {
                # code...
                Alert::success('Role Berhasil Diupdate', 'Success');
                return redirect('home/role');
            } else {
                Alert::error('Gagal Update Role', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail(base64_decode($id));
        DB::table('role_has_permit')->where('role_id', $role->id)->delete();
        $role->delete();

        Alert::success('Role Berhasil Dihapus', 'Success');
        return redirect()->route('role.index');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Alert;
use Validator;
use Access;
use File;
use Auth;
use App\RegisterOfficer;
use App\City;
use DB;

class RegisterOfficerController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->id;

            $akses = Access::getUserAccess($this->user,2);

            $this->permit = $akses->permit_access;
                
            if($akses->permit_access == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if (Auth::user()->roles->role_name == "Superuser" || Auth::user()->roles->role_name == "Admin Pusat" || Auth::user()->roles->role_name == "Staff Pusat") {
            # code...
            $data = RegisterOfficer::orderBy('name', 'asc')->where('is_deleted', 'N')->get();
        } else {
            $data = RegisterOfficer::orderBy('name', 'asc')->where('is_deleted', 'N')->where('city_id', Auth::user()->city_id)->get();
        }
        return view('register_officer.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $code = $this->format_code();
        $city = City::orderBy('city_name', 'asc')->get();
        return view('register_officer.create', compact('code', 'city'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'name' => 'required',
            'code' => 'required',
            'nik' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'date' => 'required',
            'phone' => 'required',
            'city' => 'required',
            'image' => 'image|mimes:jpeg,jpg,png'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $cek = RegisterOfficer::where('email', $request->email)->get();
            if (count($cek) <= 0) {
                # code...
                if ($request->hasFile('image')) {
                    # code...
                    $create = RegisterOfficer::create([
                        'ro_code' => $request->code,
                        'client_id' => $request->kanca,
                        'name' => $request->name,
                        'nik' => $request->nik,
                        'email' => $request->email,
                        'password' => bcrypt($request->password),
                        'birthdate' => $request->date,
                        'phone' => $request->phone,
                        'image' => Self::savePhoto($request->file('image')),
                        'city_id' => $request->city,
                        'is_deleted' => 'N'
                    ]);

                    if (is_array($request->uker)) {
                        # code...
                        for ($i=0; $i < count($request->uker); $i++) { 
                            # code...
                            DB::table('ro_has_uker')->insert([
                                'ro_id' => $create->id,
                                'uker_id' => $request->uker[$i]
                            ]);
                        }
                    } else {
                        DB::table('ro_has_uker')->insert([
                            'ro_id' => $create->id,
                            'uker_id' => $request->uker
                        ]);
                    }

                    if ($create) {
                        # code...
                        Alert::success('RO Berhasil Dibuat', 'Success');
                        return redirect('home/register-officer');
                    } else {
                        Alert::error('Gagal Membuat RO', 'Error');
                        return redirect()->back();
                    }
                } else {
                    $create = RegisterOfficer::create([
                        'ro_code' => $request->code,
                        'client_id' => $request->kanca,
                        'name' => $request->name,
                        'nik' => $request->nik,
                        'email' => $request->email,
                        'password' => bcrypt($request->password),
                        'birthdate' => $request->date,
                        'phone' => $request->phone,
                        'image' => null,
                        'city_id' => $request->city,
                        'is_deleted' => 'N'
                    ]);

                    if (is_array($request->uker)) {
                        # code...
                        for ($i=0; $i < count($request->uker); $i++) { 
                            # code...
                            DB::table('ro_has_uker')->insert([
                                'ro_id' => $create->id,
                                'uker_id' => $request->uker[$i]
                            ]);
                        }
                    } else {
                        DB::table('ro_has_uker')->insert([
                            'ro_id' => $create->id,
                            'uker_id' => $request->uker
                        ]);
                    }

                    if ($create) {
                        # code...
                        Alert::success('RO Berhasil Dibuat', 'Success');
                        return redirect('home/register-officer');
                    } else {
                        Alert::error('Gagal Membuat RO', 'Error');
                        return redirect()->back();
                    }
                }
            } else {
                Alert::info('Email Sudah Terdaftar', 'Info');
                return redirect()->back();
            }
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('register_officer.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $data = RegisterOfficer::where('id', base64_decode($id))->first();
        $city = City::orderBy('city_name', 'asc')->get();
        return view('register_officer.edit', compact('data', 'city'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $valid = Validator::make($request->all(), [
            'name' => 'required',
            'code' => 'required',
            'nik' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'date' => 'required',
            'phone' => 'required',
            'city' => 'required',
            'image' => 'image|mimes:jpeg,jpg,png'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $exists = RegisterOfficer::where('id', base64_decode($id))->first();
            if ($request->hasFile('image')) {
                # code...
                Self::deletePhoto($exists['image']);
                $create = RegisterOfficer::where('id', base64_decode($id))->update([
                    'ro_code' => $request->code,
                    'client_id' => $request->kanca,
                    'name' => $request->name,
                    'nik' => $request->nik,
                    'email' => $request->email,
                    'password' => bcrypt($request->password),
                    'birthdate' => $request->date,
                    'phone' => $request->phone,
                    'image' => Self::savePhoto($request->file('image')),
                    'city_id' => $request->city,
                    'is_deleted' => 'N'
                ]);

                DB::table('ro_has_uker')->where('ro_id', base64_decode($id))->delete();
                if (is_array($request->uker)) {
                    # code...
                    for ($i=0; $i < count($request->uker); $i++) { 
                        # code...
                        DB::table('ro_has_uker')->insert([
                            'ro_id' => base64_decode($id),
                            'uker_id' => $request->uker[$i]
                        ]);
                    }
                } else {
                    DB::table('ro_has_uker')->insert([
                        'ro_id' => base64_decode($id),
                        'uker_id' => $request->uker
                    ]);
                }

                if ($create) {
                    # code...
                    Alert::success('RO Berhasil Diupdate', 'Success');
                    return redirect('home/register-officer');
                } else {
                    Alert::error('Gagal Update RO', 'Error');
                    return redirect()->back();
                }
            } else {
                $create = RegisterOfficer::where('id', base64_decode($id))->update([
                    'ro_code' => $request->code,
                    'client_id' => $request->kanca,
                    'name' => $request->name,
                    'nik' => $request->nik,
                    'email' => $request->email,
                    'password' => bcrypt($request->password),
                    'birthdate' => $request->date,
                    'phone' => $request->phone,
                    'image' => $exists['image'],
                    'city_id' => $request->city,
                    'is_deleted' => 'N'
                ]);

                DB::table('ro_has_uker')->where('ro_id', base64_decode($id))->delete();
                if (is_array($request->uker)) {
                    # code...
                    for ($i=0; $i < count($request->uker); $i++) { 
                        # code...
                        DB::table('ro_has_uker')->insert([
                            'ro_id' => base64_decode($id),
                            'uker_id' => $request->uker[$i]
                        ]);
                    }
                } else {
                    DB::table('ro_has_uker')->insert([
                        'ro_id' => base64_decode($id),
                        'uker_id' => $request->uker
                    ]);
                }

                if ($create) {
                    # code...
                    Alert::success('RO Berhasil Diupdate', 'Success');
                    return redirect('home/register-officer');
                } else {
                    Alert::error('Gagal Update RO', 'Error');
                    return redirect()->back();
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $del = RegisterOfficer::where('id', base64_decode($id))->update([
            'is_deleted' => 'Y'
        ]);

        DB::table('ro_has_uker')->where('ro_id', base64_decode($id))->delete();
        DB::table('task_ro')->where('user_id', base64_decode($id))->delete();

        if ($del) {
            # code...
            Alert::success('RO Berhasil Dihapus', 'Success');
            return redirect()->back();
        } else {
            Alert::error('RO Gagal Dihapus', 'Error');
            return redirect()->back();
        }
    }

    public function getListKanca(Request $request)
    {
        $inv = DB::table('clients')->where('custom_value2', $request->id)->get();

        $data = "<option value='0'>-- Pilih Kanca --</option>";
        foreach ($inv as $row) {
            $data .= "<option value='".$row->id."'>".$row->name."</option>";
        }

        $response['type']       = 'spbe';
        $response['content']    = $data;
        $response['param']      = '';
        return response($response);
    }

    public function getListUker(Request $request)
    {
        $inv = DB::table('uker')->where('client_id', $request->id)->get();

        $data = "<option value='0'>-- Pilih Uker --</option>";
        foreach ($inv as $row) {
            $data .= "<option value='".$row->id."'>".$row->uker_name."</option>";
        }

        $response['type']       = 'spbe';
        $response['content']    = $data;
        $response['param']      = '';
        return response($response);
    }

    protected function savePhoto($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'user';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $create['image'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $create['image'];
    }

    protected function deletePhoto($photo)
    {
        File::delete($photo);
        return $photo;
    }

    protected function format_code()
    {
        $code = '';
        $year =date('y');

        switch (date('m')) {
            case '01': $month = "A"; break;
            case '02': $month = "B"; break;
            case '03': $month = "C"; break;
            case '04': $month = "D"; break;
            case '05': $month = "E"; break;
            case '06': $month = "F"; break;
            case '07': $month = "G"; break;
            case '08': $month = "H"; break;
            case '09': $month = "I"; break;
            case '10': $month = "J"; break;
            case '11': $month = "K"; break;
            case '12': $month = "L"; break;
        }

        $likes = 'RO-'.$year.$month;
        $findMemo = RegisterOfficer::select('ro_code')->where('ro_code', 'LIKE', '%'.$likes.'%')->orderBy('id', 'desc')->first();

        $number = 0;

        if (empty($findMemo)) {
            $number = sprintf("%0" . 4 . "d", $number + 1);
            $code = 'RO-'.$year.$month.$number;
            return $code;
        }

        $number = intval(substr($findMemo->ro_code, -4));
        $number = sprintf("%0" . 4 . "d", $number + 1);
        $code = 'RO-'.$year.$month.$number;
        return $code;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Alert;
use Auth;
use Validator;
use Access;
use DB;
use App\User;
use App\City;
use App\Client;

class ClientController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->id;

            $akses = Access::getUserAccess($this->user,3);

            $this->permit = $akses->permit_access;
                
            if($akses->permit_access == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if (Auth::user()->roles->role_name == "Superuser" || Auth::user()->roles->role_name == "Admin Pusat" || Auth::user()->roles->role_name == "Staff Pusat") {
            $data = DB::table('uker as a')
                ->select('a.*', 'c.city_name', 'b.name')
                ->join('clients as b', 'b.id', 'a.client_id')
                ->join('cities as c', 'c.city_id', 'a.city_id')
                ->orderBy('a.uker_name', 'asc')
                ->get();
        } else {
            $data = DB::table('uker as a')
                ->select('a.*', 'c.city_name', 'b.name')
                ->join('clients as b', 'b.id', 'a.client_id')
                ->join('cities as c', 'c.city_id', 'a.city_id')
                ->where('a.city_id', Auth::user()->city_id)
                ->orderBy('a.uker_name', 'asc')
                ->get();
        }

        return view('client.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if (Auth::user()->roles->role_name == "Superuser" || Auth::user()->roles->role_name == "Admin Pusat" || Auth::user()->roles->role_name == "Staff Pusat") {
            # code...
            $client = Client::select('id', 'name')->get();
            $city = City::select('city_id', 'city_name')->get();
        } else {
            $client = Client::select('id', 'name')->where('custom_value2', Auth::user()->city_id)->get();
            $city = City::select('city_id', 'city_name')->where('city_id', Auth::user()->city_id)->get();
        }
        return view('client.create', compact('city', 'client'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'city_id' => 'required',
            'kanca_id' => 'required',
            'uker_name' => 'required',
            'phone' => 'required',
            'address' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {

            $add = DB::table('uker')->insert([
                'client_id' => $request->kanca_id,
                'city_id' => (Auth::user()->city_id == null) ? $request->city_id : Auth::user()->city_id,
                'uker_name' => $request->uker_name,
                'uker_phone' => $request->phone,
                'uker_address' => $request->address
            ]);

            if ($add) {
                # code...
                Alert::success('Unit Kerja Berhasil Dibuat', 'Success');
                return redirect('home/client');
            } else {
                Alert::error('Gagal Membuat Unit Kerja', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        return view('client.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $data = DB::table('uker as a')
                ->select('a.*', 'c.city_name', 'b.name')
                ->join('clients as b', 'b.id', 'a.client_id')
                ->join('cities as c', 'c.city_id', 'a.city_id')
                ->where('a.id', base64_decode($id))
                ->first();
                
        $client = Client::select('id', 'name')->get();
        $city = City::select('city_id', 'city_name')->get();
        return view('client.edit', compact('data', 'client', 'city'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $valid = Validator::make($request->all(), [
            'city_id' => 'required',
            'kanca_id' => 'required',
            'uker_name' => 'required',
            'phone' => 'required',
            'address' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $add = DB::table('uker')->where('id', base64_decode($id))->update([
                'client_id' => $request->kanca_id,
                'city_id' => $request->city_id,
                'uker_name' => $request->uker_name,
                'uker_phone' => $request->phone,
                'uker_address' => $request->address
            ]);

            if ($add) {
                # code...
                Alert::success('Unit Kerja Berhasil Diperbarui', 'Success');
                return redirect('home/client');
            } else {
                Alert::error('Gagal Perbarui Unit Kerja', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $del = DB::table('uker')->where('id', base64_decode($id))->delete();

        if ($del) {
            # code...
            Alert::success('Unit Kerja Berhasil Dihapus', 'Success');
            return redirect('home/client');
        } else {
            Alert::error('Gagal Menghapus Unit Kerja', 'Error');
            return redirect()->back();
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Access;
use Alert;
use Validator;
use Excel;

class ReportController extends Controller
{
    //
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->id;

            $akses = Access::getUserAccess($this->user,8);

            $this->permit = $akses->permit_access;
                
            if($akses->permit_access == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    public function index()
    {
    	return view('report.index');
    }

    public function doPostReport(Request $request)
    {
    	$valid = Validator::make($request->all(), [
    		'month' => 'required',
    		'year' => 'required'
    	]);

    	if ($valid->fails()) {
    		# code...
    		Alert::info('Form Tidak Lengkap', 'Info');
    		return redirect()->back();
    	} else {
    		$month = $request->month;
    		$year = $request->year;

            $data = DB::table('task_ro')
                    ->select('task_ro.*', 'a.title', 'b.name', 'b.ro_code')
                    ->join('task_types as a', 'a.id', 'task_ro.task_type_id')
                    ->join('register_officers as b', 'b.id', 'task_ro.user_id')
                    ->whereMonth('task_ro.created_at', $request->month)
                    ->whereYear('task_ro.created_at', $request->year)
                    ->where('task_ro.is_deleted', 'N')
                    ->orderBy('task_ro.for_date', 'desc')
                    ->get();

    		// $data = DB::table('task_ro')
      //               ->select('task_ro.*', 'a.title', 'b.name', 'b.ro_code', 'c.name as client_name')
      //               ->join('clients as c', 'c.id', 'task_ro.client_id')
      //               ->join('task_types as a', 'a.id', 'task_ro.task_type_id')
      //               ->join('register_officers as b', 'b.id', 'task_ro.user_id')
      //               ->whereMonth('task_ro.created_at', $request->month)
      //               ->whereYear('task_ro.created_at', $request->year)
      //               ->where('task_ro.is_deleted', 'N')
      //               ->get();

    		$excel = Excel::create('Laporan PKSS Bulan '.$month.' Tahun '.$year, function ($excel) use ($data, $month, $year) {

	            $excel->sheet('Laporan 1', function ($sheet) use ($data, $month, $year) {
	                $sheet->loadView('export.excel', compact('data', 'month', 'year'));
	            });
	            
	        })->export('xls');
    	}
    }
}

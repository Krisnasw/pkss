<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Alert;
use Validator;
use Auth;
use DB;
use App\Role;
use App\Permit;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data = Permit::all();
        return view('permission.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $roles = Role::get();
        return view('permission.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'name' => 'required|max:40'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $name = $request['name'];
            $permission = new Permit();
            $permission->permit_access = $name;
            $permission->save();

            if (isset($request->roles)) {
                # code...
                foreach($request->roles as $role) {
                    DB::table('role_has_permit')->insert([
                        'role_id' => $role,
                        'permit_id' => $permission->id
                    ]);
                }
            }

            if ($permission) {
                # code...
                Alert::success('Permission Berhasil Dibuat', 'Success');
                return redirect('home/permissions');
            } else {
                Alert::error('Gagal Membuat Permission', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        return redirect('home/permissions');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $data = Permit::findOrFail(base64_decode($id));
        return view('permission.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $valid = Validator::make($request->all(), [
            'name' => 'required|max:40'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $name = $request['name'];
            $permission = Permit::findOrFail(base64_decode($id));
            $permission->permit_access = $name;
            $permission->save();

            if ($permission) {
                # code...
                Alert::success('Permission Berhasil Diupdate', 'Success');
                return redirect('home/permissions');
            } else {
                Alert::error('Gagal Update Permission', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $del = Permit::findOrFail(base64_decode($id));
        DB::table('role_has_permit')->where('permit_id', $del->id)->delete();
        if ($del->delete()) {
            # code...
            Alert::success('Permission Berhasil Dihapus', 'Success');
            return redirect()->back();
        } else {
            Alert::error('Gagal Hapus Permission', 'Error');
            return redirect()->back();
        }
    }
}

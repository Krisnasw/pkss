<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisterOfficer extends Model
{
    //
    protected $table = 'register_officers';
    protected $fillable = ['ro_code', 'name', 'nik', 'email', 'password', 'birthdate', 'phone', 'image', 'city_id', 'uker_id', 'client_id'];
    protected $hidden = [
        'password'
    ];

    public function city()
    {
    	return $this->belongsTo('App\City', 'city_id', 'city_id');
    }
}

<?php

namespace App\Helper;

use DB;
use App\User;

class Access {

	public static function getUserAccess($id, $permit_id) {
		$query = User::select('permits.permit_access')
					->join('role_has_permit', 'role_has_permit.role_id', 'users.role_id')
					->join('permits', 'permits.id', 'role_has_permit.permit_id')
					->where('users.id', $id)
					->where('role_has_permit.permit_id', $permit_id)
					->get();
		$result = null;
		foreach($query as $row) $result = ($row);
		return $result;
	}

	public static function getUkerData($id) {
		$query = DB::table('ro_has_uker as a')
					->select('a.*', 'b.uker_name')
					->join('uker as b', 'b.id', 'a.uker_id')
					->where('a.ro_id', $id)->get();
		return $query;
	}

	public static function getDetailClient($id) {
		$query = DB::table('clients')
					->select('name')
					->where('id', $id)
					->first();
		return $query->name;
	}

	public static function getDetailUker($id) {
		$query = DB::table('uker')
					->select('uker_name')
					->where('id', $id)
					->first();
		return $query->uker_name;
	}

	public static function getListDokumenByTask($id) {
		$query = DB::table('document_ro')
					->where('task_ro_id', $id)
					->get();
		$result = null;
		foreach ($query as $row) $result = ($row);
		return $query;
	}

}
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['XSS', 'guest']], function () {
	Route::get('/', 'AuthController@getLogin')->name('login');
	Route::post('/', 'AuthController@doLogin');
});

Route::group(['middleware' => ['XSS', 'auth'], 'prefix' => 'home'], function () {
	Route::resource('client', 'ClientController');
    Route::resource('permissions', 'PermissionController');
    
    Route::resource('register-officer', 'RegisterOfficerController');
    Route::post('register-officer/get-kanca', 'RegisterOfficerController@getListKanca');
    Route::post('register-officer/get-uker', 'RegisterOfficerController@getListUker');

    Route::resource('role', 'RoleController');
    Route::resource('user', 'SuperuserController');
    Route::resource('jenis-tugas', 'TaskTypeController');

    Route::resource('task', 'TaskController');
    Route::post('task/get-invoice', 'TaskController@getInvoiceByClient');
    Route::post('task/get-kanca', 'TaskController@getListKanca');
    Route::post('task/get-ro', 'TaskController@getListRo');
    Route::post('task/get-uker', 'TaskController@getListUker');

    Route::resource('pgs', 'PgsController');
    Route::resource('news', 'NewsController');
    Route::resource('list-pgs', 'ListPgsController');
    Route::resource('client-additions', 'ClientAddController');
    Route::resource('worker-additions', 'WorkerController');
    Route::resource('city', 'CityController');

    // Report
    Route::get('report', 'ReportController@index');
    Route::post('report', 'ReportController@doPostReport');

    // User Profile
    Route::get('main', 'DashboardController@index');
    Route::get('logout', 'DashboardController@doLogout');
    Route::get('edit-profile', 'DashboardController@getEditProfile');
    Route::post('edit-profile', 'DashboardController@doUpdateProfile');
    Route::get('change-password', 'DashboardController@getChangePassword');
    Route::post('change-password', 'DashboardController@doChangePassword');
});

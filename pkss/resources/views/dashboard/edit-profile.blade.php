@extends('template.main')

@section('title')
PKSS - Edit Profile
@stop

@section('style')

@stop

@section('content')
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header page-header-light">
		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">PKSS</span> - Edit Profile</h4>
				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>

		<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
			<div class="d-flex">
				<div class="breadcrumb">
					<a href="{{ url('home/main') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
					<span class="breadcrumb-item active">Edit Profile</span>
				</div>

				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>
	</div>
	<!-- /page header -->

	<div class="content">
		<!-- 2 columns form -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Edit Profile</h5>
				<div class="header-elements">
					<div class="list-icons">
	            		<a class="list-icons-item" data-action="collapse"></a>
	            		<a class="list-icons-item" data-action="reload"></a>
	            		<a class="list-icons-item" data-action="remove"></a>
	            	</div>
	        	</div>
			</div>

			<div class="card-body">
				{!! Form::open(['method' => 'POST', 'action' => 'DashboardController@doUpdateProfile']) !!}

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Nama Depan:</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="first_name" placeholder="Bejo" value="{!! Auth::user()->first_name !!}">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Nama Belakang:</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="last_name" placeholder="Suprapto" value="{!! Auth::user()->last_name !!}">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Email:</label>
						<div class="col-lg-9">
							<input type="email" class="form-control" placeholder="yourmail@mail.com" name="email" value="{!! Auth::user()->email !!}">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Phone:</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" placeholder="+62" name="phone" value="{!! Auth::user()->phone !!}">
						</div>
					</div>

					<div class="text-right">
						<button type="button" class="btn btn-danger" onclick="window.history.go(-1); return false;">Cancel <i class="icon-cancel-circle2 ml-2"></i></button>
						<button type="submit" class="btn btn-primary">Submit form <i class="icon-paperplane ml-2"></i></button>
					</div>

				{!! Form::close() !!}
			</div>
		</div>
		<!-- /2 columns form -->
	</div>
</div>
@stop

@section('script')
<script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
@stop
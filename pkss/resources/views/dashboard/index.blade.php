@extends('template.main')

@section('title')
PKSS - Dashboard
@stop

@section('style')
<style type="text/css">
    #mymap {
      border: 0px;
      width: 1635px;
      height: 750px;
    }
</style>
@stop

@section('content')
<div class="content-wrapper">

	<!-- Page header -->
	<div class="page-header page-header-light">
		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Dashboard</h4>
				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>

		<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
			<div class="d-flex">
				<div class="breadcrumb">
					<a href="{{ url('home/main') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
					<span class="breadcrumb-item active">Dashboard</span>
				</div>

				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>
	</div>
	<!-- /page header -->


	<!-- Content area -->
	<div class="content">

		<!-- Main charts -->
		<div class="row">
			<div class="card card-body">
				<div class="row text-center">
					<div class="col-3">
						<p><i class="icon-users2 icon-2x d-inline-block text-info"></i></p>
						<h5 class="font-weight-semibold mb-0">{!! $data['ro'] !!}</h5>
						<span class="text-muted font-size-sm">Total RO</span>
					</div>

					<div class="col-3">
						<p><i class="icon-user icon-2x d-inline-block text-warning"></i></p>
						<h5 class="font-weight-semibold mb-0">{!! $data['client'] !!}</h5>
						<span class="text-muted font-size-sm">Total Client</span>
					</div>

					<div class="col-3">
						<p><i class="icon-task icon-2x d-inline-block text-success"></i></p>
						<h5 class="font-weight-semibold mb-0">{!! $data['task'] !!}</h5>
						<span class="text-muted font-size-sm">Total Tugas</span>
					</div>

					<div class="col-3">
						<p><i class="icon-task icon-2x d-inline-block text-success"></i></p>
						<h5 class="font-weight-semibold mb-0">{!! $data['types'] !!}</h5>
						<span class="text-muted font-size-sm">Total Jenis Tugas</span>
					</div>
				</div>
			</div>

			<div id="mymap"></div>
		</div>
		<!-- /main charts -->

	</div>
	<!-- /content area -->


	<!-- Footer -->
	@include('template.footer')
	<!-- /footer -->

	</div>
@stop

@section('script')
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyARMENmBkxdeTgE14jl-o8Z5cBfo14QvoI&libraries=places"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.24/gmaps.js"></script>

<script type="text/javascript">

    var locations = {!! $map !!};

    var mymap = new GMaps({
      el: '#mymap',
      lat: -7.257472,
      lng: 112.752090,
      zoom: 10
    });

    $.each( locations, function( index, value ) {

      mymap.addMarker({
        lat: value.geo_lat,
        lng: value.geo_long,
        title: value.name,
        click: function(e) {
          swal('Lokasi RO : '+value.name, '', 'info');
        }
      });

   });

</script>
@stop
@extends('template.main')

@section('title')
PKSS - Office User
@stop

@section('style')

@stop

@section('content')
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header page-header-light">
		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">PKSS</span> - Office User</h4>
				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>

		<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
			<div class="d-flex">
				<div class="breadcrumb">
					<a href="{{ url('home/main') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
					<a href="#" class="breadcrumb-item">User Management</a>
					<span class="breadcrumb-item active">Office User</span>
				</div>

				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>
	</div>
	<!-- /page header -->

	<div class="content">
		<!-- 2 columns form -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Buat Office User</h5>
				<div class="header-elements">
					<div class="list-icons">
	            		<a class="list-icons-item" data-action="collapse"></a>
	            		<a class="list-icons-item" data-action="reload"></a>
	            		<a class="list-icons-item" data-action="remove"></a>
	            	</div>
	        	</div>
			</div>

			<div class="card-body">
				{!! Form::open(['method' => 'POST', 'route' => 'user.store']) !!}

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Nama Depan:</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="first_name" placeholder="Bejo">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Nama Belakang:</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="last_name" placeholder="Suprapto">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Email:</label>
						<div class="col-lg-9">
							<input type="email" class="form-control" placeholder="yourmail@mail.com" name="email">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Phone:</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" placeholder="+62" name="phone">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Role:</label>
						<div class="col-lg-9">
							<select class="form-control select-search" name="roles" id="roles">
								<option>- Pilih Role -</option>
								@foreach($role as $row)
								<option value="{!! $row->id !!}" name={!! $row->role_name !!}>{!! $row->role_name !!}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group row" id="penempatan">
						<label class="col-lg-3 col-form-label">Penempatan:</label>
						<div class="col-lg-9">
							<select class="form-control select-search" name="city" placeholder="Pilih Penempatan">
								@foreach($city as $row)
								<option value="{!! $row['city_id'] !!}">{!! $row['city_name'] !!}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Password:</label>
						<div class="col-lg-9">
							<input type="password" class="form-control" placeholder="*******" name="password">
						</div>
					</div>

					<div class="text-right">
						<button type="button" class="btn btn-danger" onclick="window.history.go(-1); return false;">Cancel <i class="icon-cancel-circle2 ml-2"></i></button>
						<button type="submit" class="btn btn-primary">Submit form <i class="icon-paperplane ml-2"></i></button>
					</div>

				{!! Form::close() !!}
			</div>
		</div>
		<!-- /2 columns form -->
	</div>
</div>
@stop

@section('script')
<script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('global_assets/js/demo_pages/form_select2.js') }}"></script>
<script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
<script type="text/javascript">
	$(function() {
		$("#penempatan").hide();
		$('#roles').change(function(){
		    if($('#roles').val() == '3') {
		        $('#penempatan').show();
		    } else if ($('#roles').val() == '5') {
		    	$('#penempatan').show();
		    } else {
		        $('#penempatan').hide(); 
		    } 
		});
	});
</script>
@stop
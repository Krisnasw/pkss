@extends('template.main')

@section('title')
PKSS - Buat Penugasan
@stop

@section('style')

@stop

@section('content')
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header page-header-light">
		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">PKSS</span> - Penugasan</h4>
				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>

		<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
			<div class="d-flex">
				<div class="breadcrumb">
					<a href="{{ url('home/main') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
					<span class="breadcrumb-item active">Penugasan</span>
				</div>

				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>
	</div>
	<!-- /page header -->

	<div class="content">
		<!-- 2 columns form -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Buat Penugasan</h5>
				<div class="header-elements">
					<div class="list-icons">
	            		<a class="list-icons-item" data-action="collapse"></a>
	            		<a class="list-icons-item" data-action="reload"></a>
	            		<a class="list-icons-item" data-action="remove"></a>
	            	</div>
	        	</div>
			</div>

			<div class="card-body">
				{!! Form::open(['method' => 'POST', 'route' => 'task.store']) !!}
					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Pilih Jenis Tugas:</label>
						<div class="col-lg-9">
							<select class="form-control select-search" name="task" id="task">
								<option selected>- Pilih Jenis Tugas -</option>
								@foreach($type as $row)
									<option value="{!! $row->id !!}">{!! $row->title !!}</option>
								@endforeach
							</select>
						</div>
					</div>

					@if(Auth::user()->roles->role_name == "Superuser" || Auth::user()->roles->role_name == "Admin Pusat" || Auth::user()->roles->role_name == "Staff Pusat")
					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Pilih Wilayah</label>
						<div class="col-lg-9">
							<select class="form-control select-search" name="city" onchange="getListKanca(this.value);">
								<option value="0">- Pilih Wilayah -</option>
								@foreach ($city as $row)
								<option value="{!! $row['city_id'] !!}">{!! $row['city_name'] !!}</option>
								@endforeach
							</select>
						</div>
					</div>
					@else
					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Wilayah</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="city" value="{!! Auth::user()->city->city_name !!}" readonly="readonly">
						</div>
					</div>
					@endif


					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Kanca:</label>
						<div class="col-lg-9">
							@if(Auth::user()->roles->role_name == "Superuser" || Auth::user()->roles->role_name == "Admin Pusat" || Auth::user()->roles->role_name == "Staff Pusat")
								<select data-placeholder="Pilih Kanca..." class="form-control select-search" data-fouc name="client" id="select_kanca" onchange="getInvoiceByClient(this.value);">
								</select>
							@else
								<select data-placeholder="Pilih Kanca..." class="form-control select-search" data-fouc name="client" id="select_kanca" onchange="getInvoiceByClient(this.value);">
									<option value="0">- Pilih Kanca -</option>
									@foreach($client as $row)
									<option value="{!! $row->id !!}">{!! $row->name !!}</option>
									@endforeach
								</select>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Register Officer:</label>
						<div class="col-lg-9">
							<select data-placeholder="Pilih RO..." class="form-control select-search" data-fouc name="ro_id" id="select_ro" onchange="getListUker(this.value);">
							</select>
						</div>
					</div>

					<div class="form-group row" id="form_uker">
						<label class="col-lg-3 col-form-label">Pilih Unit Kerja:</label>
						<div class="col-lg-9">
							<select data-placeholder="Pilih Unit Kerja..." multiple="multiple" class="form-control select-search" data-fouc name="uker[]" id="select_uker">
							</select>
						</div>
					</div>

					<div class="form-group row" id="inv">
						<label class="col-lg-3 col-form-label">Pilih Invoice:</label>
						<div class="col-lg-9">
							<select class="form-control select-search" name="invoice" id="invoice">
								<option selected="selected" value="0">- Pilih Invoice -</option>
							</select>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Tanggal Penugasan:</label>
						<div class="col-lg-9">
							<input type="date" class="form-control" name="date">
						</div>
					</div>

					<div class="text-right">
						<button type="button" class="btn btn-danger" onclick="window.history.go(-1); return false;">Cancel <i class="icon-cancel-circle2 ml-2"></i></button>
						<button type="submit" class="btn btn-primary">Submit Tugas <i class="icon-paperplane ml-2"></i></button>
					</div>

				{!! Form::close() !!}
			</div>
		</div>
		<!-- /2 columns form -->
	</div>
</div>
@stop

@section('script')
<script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('global_assets/js/demo_pages/form_select2.js') }}"></script>
<script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
<script type="text/javascript">
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

	$(function() {
		$("#inv").hide();
		$('#task').change(function(){
		    if($('#task').val() == '3') {
		        $('#inv').show();
		        $("#form_uker").hide();
		    } else {
		    	$("#form_uker").show();
		        $('#inv').hide(); 
		    } 
		});
	});

	function getListKanca(id) {
		$.ajax({
			type: 'POST',
			url: '{{ url('home/task/get-kanca') }}',
			data: { id:id },
			dataType: 'JSON',
			success: function(data) {
				$("#select_kanca").html(data.content);
			}
		});
	}

	function getInvoiceByClient(id) {
		$.ajax({
	        type: 'POST',
	        url: '{{ url('home/task/get-invoice') }}',
	        data: { id:id },
	        dataType: 'JSON',
	        success: function(data) {
		        $('#invoice').html(data.content);
            }
        });

        $.ajax({
        	type: 'POST',
        	url: '{{ url('home/task/get-ro') }}',
        	data: { id:id },
        	dataType: 'JSON',
        	success: function(data) {
        		$('#select_ro').html(data.content);
        	}
        });
	}

	function getListUker(id) {
		$.ajax({
			type: 'POST',
			url: '{{ url('home/task/get-uker') }}',
			data: { id:id },
			dataType: 'JSON',
			success: function(data) {
				$("#select_uker").html(data.content);
			}
		});
	}
</script>
@stop
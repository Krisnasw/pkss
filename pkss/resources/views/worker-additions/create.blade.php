@extends('template.main')

@section('title')
PKSS - Buat Penambahan Pekerja
@stop

@section('content')
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header page-header-light">
		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">PKSS</span> - Penambahan Pekerja</h4>
				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>

		<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
			<div class="d-flex">
				<div class="breadcrumb">
					<a href="{{ url('home/main') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
					<span class="breadcrumb-item active">Penambahan Pekerja</span>
				</div>

				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>
	</div>
	<!-- /page header -->

	<div class="content">
		<!-- 2 columns form -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Buat Penambahan Pekerja Baru</h5>
				<div class="header-elements">
					<div class="list-icons">
	            		<a class="list-icons-item" data-action="collapse"></a>
	            		<a class="list-icons-item" data-action="reload"></a>
	            		<a class="list-icons-item" data-action="remove"></a>
	            	</div>
	        	</div>
			</div>

			<div class="card-body">
				{!! Form::open(['method' => 'POST', 'route' => 'worker-additions.store']) !!}
					@if(Auth::user()->roles->role_name == "Superuser" || Auth::user()->roles->role_name == "Admin Pusat" || Auth::user()->roles->role_name == "Staff Pusat")
					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Pilih Wilayah</label>
						<div class="col-lg-9">
							<select class="form-control select-search" name="city" onchange="getListKanca(this.value);">
								@foreach ($city as $row)
								<option value="{!! $row['city_id'] !!}">{!! $row['city_name'] !!}</option>
								@endforeach
							</select>
						</div>
					</div>
					@else
					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Wilayah</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="city" value="{!! Auth::user()->city->city_name !!}" readonly="readonly">
						</div>
					</div>
					@endif

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Kanca:</label>
						<div class="col-lg-9">
							@if(Auth::user()->roles->role_name == "Superuser" || Auth::user()->roles->role_name == "Admin Pusat" || Auth::user()->roles->role_name == "Staff Pusat")
								<select data-placeholder="Pilih Kanca..." class="form-control select-search" data-fouc name="client" id="select_kanca" onchange="getListRo(this.value);">
								</select>
							@else
								<select data-placeholder="Pilih Kanca..." class="form-control select-search" data-fouc name="client" id="select_kanca" onchange="getListRo(this.value);">
									<option value="0">- Pilih Kanca -</option>
									@foreach($kanca as $row)
									<option value="{!! $row->id !!}">{!! $row->name !!}</option>
									@endforeach
								</select>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Pilih RO:</label>
						<div class="col-lg-9">
							<select class="form-control select-search" name="ro_id" id="select_ro">
								<option value="0">- Pilih RO -</option>
							</select>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Realisasi:</label>
						<div class="col-lg-9">
							<input type="number" class="form-control" name="realisasi" min="0">
					    </div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Belum Realisasi:</label>
						<div class="col-lg-9">
							<input type="number" class="form-control" name="unrealisasi" min="0">
					    </div>
					</div>

					<div class="text-right">
						<button type="button" class="btn btn-danger" onclick="window.history.go(-1); return false;">Cancel <i class="icon-cancel-circle2 ml-2"></i></button>
						<button type="submit" class="btn btn-primary">Submit form <i class="icon-paperplane ml-2"></i></button>
					</div>
				</form>
			</div>
		</div>
		<!-- /2 columns form -->
	</div>
</div>
@stop

@section('script')
<script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('global_assets/js/demo_pages/form_select2.js') }}"></script>
<script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
<script type="text/javascript">
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function getListKanca(id) {
		$.ajax({
			type: 'POST',
			url: '{{ url('home/task/get-kanca') }}',
			data: { id:id },
			dataType: 'JSON',
			success: function(data) {
				$("#select_kanca").html(data.content);
			}
		});
	}

	function getListRo(id) {
		$.ajax({
        	type: 'POST',
        	url: '{{ url('home/task/get-ro') }}',
        	data: { id:id },
        	dataType: 'JSON',
        	success: function(data) {
        		$('#select_ro').html(data.content);
        	}
        });
	}
</script>
@stop
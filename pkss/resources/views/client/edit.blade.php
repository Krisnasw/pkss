@extends('template.main')

@section('title')
PKSS - Ubah Unit Kerja
@stop

@section('style')

@stop

@section('content')
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header page-header-light">
		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">PKSS</span> - Ubah Unit Kerja</h4>
				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>

		<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
			<div class="d-flex">
				<div class="breadcrumb">
					<a href="{{ url('home/main') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
					<a href="#" class="breadcrumb-item">User Management</a>
					<span class="breadcrumb-item active">Ubah Unit Kerja</span>
				</div>

				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>
	</div>
	<!-- /page header -->

	<div class="content">
		<!-- 2 columns form -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Ubah Unit Kerja</h5>
				<div class="header-elements">
					<div class="list-icons">
	            		<a class="list-icons-item" data-action="collapse"></a>
	            		<a class="list-icons-item" data-action="reload"></a>
	            		<a class="list-icons-item" data-action="remove"></a>
	            	</div>
	        	</div>
			</div>

			<div class="card-body">
				{!! Form::model($data, ['method' => 'PATCH', 'route' => ['client.update', base64_encode($data->id)]]) !!}
					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Pilih Wilayah:</label>
						<div class="col-lg-9">
							<select class="form-control select-search" name="city_id">
								<option value="0">- Pilih Wilayah -</option>
								@foreach($city as $row)
								<option value="{!! $row->city_id !!}" {!! ($data->city_name == $row->city_name) ? 'selected' : '' !!}>{!! $row->city_name !!}</option>
								@endforeach
							</select>
						</div>
					</div>
					
					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Pilih Kanca:</label>
						<div class="col-lg-9">
							<select class="form-control select-search" name="kanca_id">
								<option value="0">- Pilih Kanca -</option>
								@foreach($client as $row)
								<option value="{!! $row->id !!}" {!! ($data->client_id == $row->id) ? 'selected' : '' !!}>{!! $row->name !!}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Nama Uker:</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="uker_name" value="{!! $data->uker_name !!}">
					    </div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">No. Telepon:</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="phone" value="{!! $data->uker_phone !!}">
					    </div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Alamat:</label>
						<div class="col-lg-9">
							<input type="text" name="address" class="form-control" value="{!! $data->uker_address !!}">
						</div>
					</div>

					<div class="text-right">
						<button type="button" class="btn btn-danger">Cancel<i class="icon-cancel-circle2 ml-2"></i></button>
						<button type="submit" class="btn btn-primary">Submit form <i class="icon-paperplane ml-2"></i></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
		<!-- /2 columns form -->
	</div>
</div>
@stop

@section('script')
<script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('global_assets/js/demo_pages/form_select2.js') }}"></script>
<script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
@stop
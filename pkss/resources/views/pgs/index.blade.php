@extends('template.main')

@section('title')
PKSS - Pengganti Sementara
@stop

@section('style')

@stop

@section('content')
<!-- Main content -->
<div class="content-wrapper">

	<!-- Page header -->
	<div class="page-header page-header-light">
		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">PKSS</span> - Pengganti Sementara</h4>
				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>

		<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
			<div class="d-flex">
				<div class="breadcrumb">
					<a href="{{ url('home/main') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
					<span class="breadcrumb-item active">Pengganti Sementara</span>
				</div>

				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>
	</div>
	<!-- /page header -->

	<!-- Content area -->
	<div class="content">

		<!-- Basic datatable -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">List Pengganti Sementara</h5>
				<div class="header-elements">
					<div class="list-icons">
                		<a class="list-icons-item" data-action="collapse"></a>
                		<a class="list-icons-item" data-action="reload"></a>
                		<a class="list-icons-item" data-action="remove"></a>
                	</div>
            	</div>
			</div>

			<div class="card-body">
				<a href="{{ url('home/pgs/create') }}" class="btn bg-teal-400 btn-labeled btn-labeled-left"><b><i class="icon-add"></i></b> Buat PGS Baru</a>
			</div>

			<table class="table datatable-basic">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama PGS</th>
						<th>Jenis</th>
						<th>Status</th>
						<th class="text-center">Aksi</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data as $index => $row)
						<tr>
							<td>{!! ++$index !!}</td>
							<td>{!! $row->pgs_name !!}</td>
							<td>{!! ($row->type == "external") ? "External" : "Internal" !!}</td>
							<td>{!! ($row->status == "Y") ? "Available" : "Unavailable" !!}</td>
							<td class="text-center">
								<div class="list-icons">
									<div class="dropdown">
										<a href="#" class="list-icons-item" data-toggle="dropdown">
											<i class="icon-menu9"></i>
										</a>
										<div class="dropdown-menu dropdown-menu-right">
											<a href="{{ url('home/pgs') }}/{!! base64_encode($row->id) !!}/edit" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>
											<a href="#" class="dropdown-item" data-toggle="modal" data-target="#mdl_danger{{{ $row->id }}}"><i class="icon-trash"></i> Delete</a>
										</div>
									</div>
								</div>
							</td>
						</tr>

						<!-- Danger modal -->
						<div id="mdl_danger{{{ $row->id }}}" class="modal fade" tabindex="-1">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header bg-danger">
										<h6 class="modal-title">Hapus Data</h6>
										<button type="button" class="close" data-dismiss="modal">&times;</button>
									</div>

									<div class="modal-body">
										<p>Apakah Anda Yakin Menghapus <b>{!! $row->pgs_name !!}</b> ?</p>
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">Tidak</button>
                                        {!! Form::open(['method' => 'DELETE', 'route' => array('pgs.destroy', base64_encode($row->id))]) !!}
                                            {!! Form::submit("Ya", array('class' => 'btn bg-danger')) !!}
                                        {!! Form::close() !!}
									</div>
								</div>
							</div>
						</div>
						<!-- /default modal -->
					@endforeach
				</tbody>
			</table>
		</div>
		<!-- /basic datatable -->

	</div>
	<!-- /content area -->

</div>
<!-- /main content -->
@stop

@section('script')
<script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ asset('global_assets/js/demo_pages/datatables_basic.js') }}"></script>
@stop

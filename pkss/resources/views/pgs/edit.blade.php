@extends('template.main')

@section('title')
PKSS - Edit PGS
@stop

@section('content')
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header page-header-light">
		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">PKSS</span> - PGS</h4>
				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>

		<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
			<div class="d-flex">
				<div class="breadcrumb">
					<a href="{{ url('home/main') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
					<span class="breadcrumb-item active">PGS</span>
				</div>

				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>
	</div>
	<!-- /page header -->

	<div class="content">
		<!-- 2 columns form -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Edit PGS</h5>
				<div class="header-elements">
					<div class="list-icons">
	            		<a class="list-icons-item" data-action="collapse"></a>
	            		<a class="list-icons-item" data-action="reload"></a>
	            		<a class="list-icons-item" data-action="remove"></a>
	            	</div>
	        	</div>
			</div>

			<div class="card-body">
				{!! Form::model($data, ['method' => 'PATCH', 'route' => ['pgs.update', base64_encode($data->id)]]) !!}
					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Nama PGS:</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="name" value="{!! $data->pgs_name !!}">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Jenis:</label>
						<div class="col-lg-9">
							<select class="form-control select-search" name="jenis">
								<option value="0">- Pilih Jenis -</option>
								<option value="external" {!! ($data->type == "external") ? 'selected' : '' !!}>External</option>
								<option value="internal" {!! ($data->type == "internal") ? 'selected' : '' !!}>Internal</option>
							</select>
					    </div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Status:</label>
						<div class="col-lg-9">
							<select class="form-control select-search" name="status">
								<option value="0">- Pilih Status -</option>
								<option value="Y" {!! ($data->type == "Y") ? 'selected' : '' !!}>Available</option>
								<option value="N" {!! ($data->type == "N") ? 'selected' : '' !!}>Unavailable</option>
							</select>
					    </div>
					</div>

					<div class="text-right">
						<button type="button" class="btn btn-danger" onclick="window.history.go(-1); return false;">Cancel <i class="icon-cancel-circle2 ml-2"></i></button>
						<button type="submit" class="btn btn-primary">Submit form <i class="icon-paperplane ml-2"></i></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
		<!-- /2 columns form -->
	</div>
</div>
@stop

@section('script')
<script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
@stop
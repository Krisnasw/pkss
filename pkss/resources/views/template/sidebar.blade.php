<div class="navbar navbar-expand-md navbar-dark bg-indigo navbar-static">
	<div class="navbar-brand">
		<a href="index.html" class="d-inline-block">
			PKSS
		</a>
	</div>

	<div class="d-md-none">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
			<i class="icon-tree5"></i>
		</button>
		<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
			<i class="icon-paragraph-justify3"></i>
		</button>
	</div>

	<div class="collapse navbar-collapse" id="navbar-mobile">
		<ul class="navbar-nav">
			<li class="nav-item">
				<a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
					<i class="icon-paragraph-justify3"></i>
				</a>
			</li>
		</ul>

		<span class="navbar-text ml-md-3">
			<span class="badge badge-mark border-orange-300 mr-2"></span>
			@if (Auth::user()->roles->role_name == "Superuser" || Auth::user()->roles->role_name == "Admin Pusat" || Auth::user()->roles->role_name == "Staff Pusat")
				Welcome, {!! Auth::user()->first_name !!} {!! Auth::user()->last_name !!}!
			@else
				Welcome, {!! Auth::user()->first_name !!} {!! Auth::user()->last_name !!} - {!! Auth::user()->city->city_name !!}
			@endif
		</span>

		<ul class="navbar-nav ml-md-auto">
			<li class="nav-item">
				<a href="{{ url('home/logout') }}" class="navbar-nav-link">
					<i class="icon-switch2"></i>
					<span class="d-md-none ml-2">Logout</span>
				</a>
			</li>
		</ul>
	</div>
</div>
<div class="sidebar sidebar-light sidebar-main sidebar-expand-md">

			<!-- Sidebar mobile toggler -->
			<div class="sidebar-mobile-toggler text-center">
				<a href="#" class="sidebar-mobile-main-toggle">
					<i class="icon-arrow-left8"></i>
				</a>
				<span class="font-weight-semibold">Navigation</span>
				<a href="#" class="sidebar-mobile-expand">
					<i class="icon-screen-full"></i>
					<i class="icon-screen-normal"></i>
				</a>
			</div>
			<!-- /sidebar mobile toggler -->

			<!-- Sidebar content -->
			<div class="sidebar-content">

				<!-- User menu -->
				<div class="sidebar-user-material">
					<div class="sidebar-user-material-body">
						<div class="card-body text-center">
							<a href="#">
								<img src="{{ Avatar::create(Auth::user()->first_name)->toBase64() }}" class="img-fluid rounded-circle shadow-1 mb-3" width="80" height="80" alt="">
							</a>
							@if (Auth::user()->roles->role_name == "Superuser" || Auth::user()->roles->role_name == "Admin Pusat" || Auth::user()->roles->role_name == "Staff Pusat")
								<h6 class="mb-0 text-white text-shadow-dark">{!! Auth::user()->first_name !!} {!! Auth::user()->last_name !!}</h6>
							@else
								<h6 class="mb-0 text-white text-shadow-dark">{!! Auth::user()->first_name !!} {!! Auth::user()->last_name !!} - {!! Auth::user()->city->city_name !!}</h6>
							@endif
						</div>
													
						<div class="sidebar-user-material-footer">
							<a href="#user-nav" class="d-flex justify-content-between align-items-center text-shadow-dark dropdown-toggle" data-toggle="collapse"><span>My account</span></a>
						</div>
					</div>

					<div class="collapse" id="user-nav">
						<ul class="nav nav-sidebar">
							<li class="nav-item">
								<a href="{{ url('home/edit-profile') }}" class="nav-link {{ request()->is('home/edit-profile') ? 'active' : '' }}">
									<i class="icon-cog5"></i>
									<span>Account settings</span>
								</a>
							</li>
							<li class="nav-item">
								<a href="{{ url('home/change-password') }}" class="nav-link {{ request()->is('home/change-password') ? 'active' : '' }}">
									<i class="icon-key"></i>
									<span>Ubah Password</span>
								</a>
							</li>
							<li class="nav-item">
								<a href="{{ url('home/logout') }}" class="nav-link">
									<i class="icon-switch2"></i>
									<span>Logout</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<!-- /user menu -->

				<!-- Main navigation -->
				<div class="card card-sidebar-mobile">
					<ul class="nav nav-sidebar" data-nav-type="accordion">

						<!-- Main -->
						<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
						<li class="nav-item">
							<a href="{{ url('home/main') }}" class="nav-link {{ request()->is('home/main') ? 'active' : '' }}">
								<i class="icon-home4"></i>
								<span>
									Dashboard
								</span>
							</a>
						</li>
						<li class="nav-item nav-item-submenu">
							<a href="#" class="nav-link"><i class="icon-user"></i> <span>User Management</span></a>

							<ul class="nav nav-group-sub" data-submenu-title="Layouts">
								<li class="nav-item {!! request()->is('home/user') ? 'active' : '' !!}">
									<a href="{{ url('home/user') }}" class="nav-link">Office User</a>
								</li>
								<li class="nav-item"><a href="{{ url('home/register-officer') }}" class="nav-link {{ request()->is('home/register-officer') ? 'active' : '' }}">Register Officer</a></li>
								<li class="nav-item"><a href="{{ url('home/client') }}" class="nav-link {{ request()->is('home/client') ? 'active' : '' }}">Unit Kerja</a></li>
								@if(Auth::user()->roles->role_name == "Superuser")
								<li class="nav-item"><a href="{{ url('home/city') }}" class="nav-link {{ request()->is('home/city') ? 'active' : '' }}">Wilayah</a></li>
								<li class="nav-item"><a href="{{ url('home/role') }}" class="nav-link {{ request()->is('home/role') ? 'active' : '' }}">Role</a></li>
								<li class="nav-item"><a href="{{ url('home/permissions') }}" class="nav-link {{ request()->is('home/permissions') ? 'active' : '' }}">Akses Menu</a></li>
								@endif
							</ul>
						</li>
						<li class="nav-item nav-item-submenu">
							<a href="#" class="nav-link"><i class="icon-task"></i> <span>Tugas</span></a>

							<ul class="nav nav-group-sub" data-submenu-title="Layouts">
								@if(Auth::user()->role_id == 1)
								<li class="nav-item"><a href="{{ url('home/jenis-tugas') }}" class="nav-link {{ request()->is('home/jenis-tugas') ? 'active' : '' }}">Jenis Tugas</a></li>
								@endif
								<li class="nav-item"><a href="{{ url('home/task') }}" class="nav-link {{ request()->is('home/task') ? 'active' : '' }}">Penugasan</a></li>
							</ul>
						</li>
						<li class="nav-item">
							<a href="{{ url('home/news') }}" class="nav-link {{ request()->is('home/news') ? 'active' : '' }}">
								<i class="icon-book"></i>
								<span>
									Berita
								</span>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('home/client-additions') }}" class="nav-link {{ request()->is('home/client-additions') ? 'active' : '' }}">
								<i class="icon-users4"></i>
								<span>
									Penambahan Client
								</span>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('home/worker-additions') }}" class="nav-link {{ request()->is('home/worker-additions') ? 'active' : '' }}">
								<i class="icon-users"></i>
								<span>
									Penambahan Pekerja
								</span>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ url('home/report') }}" class="nav-link {{ request()->is('home/report') ? 'active' : '' }}">
								<i class="icon-clippy"></i>
								<span>
									Laporan
								</span>
							</a>
						</li>
						<!-- /main -->

					</ul>
				</div>
				<!-- /main navigation -->

			</div>
			<!-- /sidebar content -->
			
		</div>
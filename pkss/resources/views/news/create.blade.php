@extends('template.main')

@section('title')
PKSS - Buat Berita
@stop

@section('style')

@stop

@section('content')
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header page-header-light">
		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">PKSS</span> - Berita</h4>
				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>

		<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
			<div class="d-flex">
				<div class="breadcrumb">
					<a href="{{ url('home/main') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
					<span class="breadcrumb-item active">Berita</span>
				</div>

				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>
	</div>
	<!-- /page header -->

	<div class="content">
		<!-- 2 columns form -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Buat Berita</h5>
				<div class="header-elements">
					<div class="list-icons">
	            		<a class="list-icons-item" data-action="collapse"></a>
	            		<a class="list-icons-item" data-action="reload"></a>
	            		<a class="list-icons-item" data-action="remove"></a>
	            	</div>
	        	</div>
			</div>

			<div class="card-body">
				{!! Form::open(['method' => 'POST', 'route' => 'news.store', 'files' => true]) !!}

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Judul Berita:</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="title" placeholder="Masukkan Judul Berita">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Author:</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="author" value="{!! Auth::user()->first_name !!} {!! Auth::user()->last_name !!}" readonly>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Deskripsi:</label>
						<div class="col-lg-9">
							<textarea class="form-control" rows="6" name="deskripsi" id="editor-full">
								
							</textarea>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Image:</label>
						<div class="col-lg-9">
							<input type="file" class="form-input-styled" name="images" accept="image/*">
						</div>
					</div>

					<div class="text-right">
						<button type="button" class="btn btn-danger" onclick="window.history.go(-1); return false;">Cancel <i class="icon-cancel-circle2 ml-2"></i></button>
						<button type="submit" class="btn btn-primary">Submit form <i class="icon-paperplane ml-2"></i></button>
					</div>

				{!! Form::close() !!}
			</div>
		</div>
		<!-- /2 columns form -->
	</div>
</div>
@stop

@section('script')
<script src="{{ asset('global_assets/js/plugins/editors/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('global_assets/js/demo_pages/editor_ckeditor.js') }}"></script>
<script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
@stop
@extends('template.export')

@section('content')
<table>
  <tr>
    <td colspan="5" align="left">
        <p style="background-color: #3498db;">LAPORAN  KEGIATAN PENGELOLAAN & PEMBINAAN UNIT BISNIS 1</p>
    </td>
  </tr>
  <tr>
    <td colspan="5" align="left">
        <p style="background-color: #3498db;">Periode Bulan : {!! date('M', strtotime($month)) !!} {!! $year !!}</p>
    </td>
  </tr>
</table>
<table class="tableku">
    <tr>
        <th bgcolor="#f1c40f" align="center">No</th>
        <th bgcolor="#f1c40f" align="center">Tanggal Kunjungan</th>
        <th bgcolor="#f1c40f" align="center">Unit Kerja BRI</th>
        <th bgcolor="#f1c40f" align="center">Kegiatan / Tujuan Kunjungan</th>
        <th bgcolor="#f1c40f" align="center">Nama & Jabatan / Job Pekerja BRI / PKSS Yang Ditemui</th>
        <th bgcolor="#f1c40f" align="center">Nama Staff Pembina</th>
    </tr>
    <tbody>
    @foreach($data as $index => $row)
        <tr align="center">
            <td align="center">{!! ++$index !!}</td>
            <td align="center">{!! $row->for_date !!}</td>
            <td align="center">{!! ($row->title == "Monitoring Invoice") ? Access::getDetailClient($row->client_id) : Access::getDetailUker($row->client_id) !!}</td>
            <td align="center">{!! (empty($row->catatan)) ? 'Tidak Ada Catatan' : $row->catatan !!}</td>
            <td align="center">{!! (empty($row->penerima)) ? 'Belum Diterima' : $row->penerima !!} {!! (empty($row->jabatan)) ? '' : "( ".$row->jabatan." )" !!}</td>
            <td align="center">{!! $row->name !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>
@stop
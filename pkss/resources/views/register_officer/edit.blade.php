@extends('template.main')

@section('title')
PKSS - Edit RO
@stop

@section('content')
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header page-header-light">
		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">PKSS</span> - Register Officer</h4>
				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>

		<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
			<div class="d-flex">
				<div class="breadcrumb">
					<a href="{{ url('home/main') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
					<span class="breadcrumb-item active">Register Officer</span>
				</div>

				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>
	</div>
	<!-- /page header -->

	<div class="content">
		<!-- 2 columns form -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Edit RO</h5>
				<div class="header-elements">
					<div class="list-icons">
	            		<a class="list-icons-item" data-action="collapse"></a>
	            		<a class="list-icons-item" data-action="reload"></a>
	            		<a class="list-icons-item" data-action="remove"></a>
	            	</div>
	        	</div>
			</div>

			<div class="card-body">
				{!! Form::model($data, ['method' => 'PATCH', 'route' => ['register-officer.update', base64_encode($data['id'])], 'files' => true]) !!}
					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Code RO:</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" value="{!! $data['ro_code'] !!}" name="code" readonly>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Nama RO:</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" placeholder="John Doe" name="name" value="{!! $data['name'] !!}">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">NIK:</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" placeholder="123" name="nik" value="{!! $data['nik'] !!}">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Email:</label>
						<div class="col-lg-9">
							<input type="email" class="form-control" placeholder="yourmail@mail.com" name="email" value="{!! $data['email'] !!}">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Password:</label>
						<div class="col-lg-9">
							<input type="password" class="form-control" placeholder="*******" name="password" value="{!! $data['password'] !!}">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Birthdate:</label>
						<div class="col-lg-9">
							<input type="date" class="form-control" placeholder="2018-01-01" name="date" value="{!! $data['birthdate'] !!}">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Phone:</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" placeholder="+62" name="phone" value="{!! $data['phone'] !!}">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Penempatan:</label>
						<div class="col-lg-9">
							<select class="form-control select-search" name="city" onchange="getListKanca(this.value);" ontoggle="getListKanca(this.value);">
								<option value="0">- Pilih Penempatan -</option>
								@foreach($city as $row)
								<option value="{!! $row['city_id'] !!}" {!! ($data['city_id'] == $row['city_id']) ? 'selected' : '' !!}>{!! $row['city_name'] !!}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Kanca:</label>
						<div class="col-lg-9">
							<select data-placeholder="Pilih Kanca..." class="form-control select-search" data-fouc name="kanca" id="select_kanca" onchange="getListUker(this.value);">
							</select>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Pilih Uker:</label>
						<div class="col-lg-9">
							<select data-placeholder="Pilih Uker..." multiple="multiple" class="form-control select" data-fouc name="uker[]" id="select_uker">
							</select>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Foto:</label>
						<div class="col-lg-9">
							<input type="file" class="form-input-styled" name="image" accept="image/*">
							<img src="{{ asset($data['image']) }}" class="img img-responsive" style="margin: 10%;">
						</div>
					</div>

					<div class="text-right">
						<button type="button" class="btn btn-danger" onclick="window.history.go(-1); return false;">Cancel <i class="icon-cancel-circle2 ml-2"></i></button>
						<button type="submit" class="btn btn-primary">Submit form <i class="icon-paperplane ml-2"></i></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
		<!-- /2 columns form -->
	</div>
</div>
@stop

@section('script')
<script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('global_assets/js/demo_pages/form_select2.js') }}"></script>
<script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
<script type="text/javascript">
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

	function getListKanca(id) {
		$.ajax({
	        type: 'POST',
	        url: '{{ url('home/register-officer/get-kanca') }}',
	        data: { id:id },
	        dataType: 'JSON',
	        success: function(data) {
		        $('#select_kanca').html(data.content);
            }
        });
	}

	function getListUker(id) {
		$.ajax({
			type: 'POST',
			url: '{{ url('home/register-officer/get-uker') }}',
			data: { id:id },
			dataType: 'JSON',
			success: function (data) {
				$('#select_uker').html(data.content);
			}
		});
	}
</script>
@stop
@extends('template.main')

@section('title')
PKSS - Buat Role Baru
@stop

@section('content')
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header page-header-light">
		<div class="page-header-content header-elements-md-inline">
			<div class="page-title d-flex">
				<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">PKSS</span> - Role</h4>
				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>

		<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
			<div class="d-flex">
				<div class="breadcrumb">
					<a href="{{ url('home/main') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
					<span class="breadcrumb-item active">Role</span>
				</div>

				<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
			</div>
		</div>
	</div>
	<!-- /page header -->

	<div class="content">
		<!-- 2 columns form -->
		<div class="card">
			<div class="card-header header-elements-inline">
				<h5 class="card-title">Buat Role Baru</h5>
				<div class="header-elements">
					<div class="list-icons">
	            		<a class="list-icons-item" data-action="collapse"></a>
	            		<a class="list-icons-item" data-action="reload"></a>
	            		<a class="list-icons-item" data-action="remove"></a>
	            	</div>
	        	</div>
			</div>

			<div class="card-body">
				{!! Form::open(['method' => 'POST', 'route' => 'role.store']) !!}
					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Nama Role:</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" placeholder="Staff" name="name">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-lg-3 col-form-label">Akses Menu:</label>
						<div class="col-lg-9">
							@foreach ($data as $permission)
					            {{ Form::checkbox('permissions[]',  $permission->id, ['class' => 'form-control']) }}
					            {{ Form::label($permission->permit_access, ucfirst($permission->permit_access)) }}<br>
					        @endforeach
					    </div>
					</div>

					<div class="text-right">
						<button type="button" class="btn btn-danger" onclick="window.history.go(-1); return false;">Cancel <i class="icon-cancel-circle2 ml-2"></i></button>
						<button type="submit" class="btn btn-primary">Submit form <i class="icon-paperplane ml-2"></i></button>
					</div>
				</form>
			</div>
		</div>
		<!-- /2 columns form -->
	</div>
</div>
@stop

@section('script')

@stop